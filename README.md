# Atlassian Connect Dashboard Item Example

This add-on demonstrates simple usage of a dashboard item for Atlassian Connect.

Get started:

1. Start up a JIRA cloud development environment following the [development setup guide](https://developer.atlassian.com/static/connect/docs/latest/guides/development-setup.html).
2. Follow the [developing locally guide](https://developer.atlassian.com/static/connect/docs/latest/developing/developing-locally.html) to expose your running add-on to the internet.
2. Ensure you have `node.js` installed, then run `npm install` to install dependencies.
3. Run `node app.js` to start the add-on, then install it in your cloud instance.
4. Add dashboard item to dashboard.

For more information, read our [guide on developing dashboard items](https://developer.atlassian.com/display/JIRADEV/Guide+-+Building+a+dashboard+item+for+a+JIRA+Connect+add-on) and the reference documentation for [Connect dashboard items](https://developer.atlassian.com/static/connect/docs/modules/jira/dashboard-item.html).